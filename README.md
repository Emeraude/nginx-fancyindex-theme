# A theme for nginx fancyindex module

This theme is an improvement of the default theme for the [fancyindex](https://github.com/aperezdc/ngx-fancyindex) module. It features a dark theme, minor CSS improvements and a custom footer providing a wget command to download files.  
It aims to be light and does not use any JavaScript.

## Installation

Put all the files present in this repository in `/etc/nginx/themes/enhanceddefault/`. You might want to put them elsewhere, but then you'd have to edit the `theme.conf` file accordingly.  
Add the following line in your nginx configuration:
```nginx
location / {
  fancyindex on;
  include themes/enhanceddefault/theme.conf; # you might want to edit this line if you've put your files elsewhere
}
```

## Improvements

This theme could do with a few improvements:
- Improve responsiveness
- Handle optional inclusion of a README file, avoiding having to edit the `header.html` or `footer.html` file whenever we need to edit some general informations
- Find a propper name for it
